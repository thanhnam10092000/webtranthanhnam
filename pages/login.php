<?php
    require '../utils/database.php';
    if(isset($_POST["register"])) {
        $_SESSION["page"] = "register";
        header("Refresh:0");
    }
    if (isset($_POST["login"])) {
        $result = $db -> query("SELECT username FROM user WHERE username='{$_POST["username"]}' AND password='{$_POST["password"]}'");
        if($result -> num_rows > 0) {
            $_SESSION["username"] = $_POST["username"];
            $_SESSION["page"] = "list";
            header("Refresh:0");
        }
    }
?>
<div class="login center">
    <div class="login-box center">
        <div class="login-logo">
            <img src="../assets/icons/shoe.svg">
        </div>
        <form class="login-info" method="POST">
            <div class="login-title">Tên người dùng:</div> 
            <input class="login-input input" type="input" name="username" placeholder="Nhập tên người dùng" required>
            <div class="login-title">Mật khẩu:</div> 
            <input class="login-input input" type="input" name="password" placeholder="Nhập mật khẩu" required>
            <button class="login-btn btn" name="login">
                Đăng nhập
            </button>
            <button class="login-btn btn" name="register" formnovalidate>
                Chưa là thành viên?
            </button>
        </form>
    </div>
</div>