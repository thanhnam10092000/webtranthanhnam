<?php
    require '../utils/database.php';
    if(isset($_POST["return"])) {
        $_SESSION["page"] = "list";
        header("Refresh:0");
    }
?>
<form class="product" method="POST">
    <div class="product-left">
        <button class="product-return btn" name="return">
            < Trở lại
        </button>
        <div class="product-img">
             <img src="../database/<?=$_SESSION["product"]?>.png">
        </div>
    </div>
    <div class="product-info">
        <?php
            $result = $db->query("SELECT * FROM giay WHERE magiay='{$_SESSION["product"]}'");

            if (mysqli_num_rows($result) > 0):
                while($row = $result->fetch_assoc()):
        ?>
        <div class="product-type">Sportswear</div>
        <div class="product-name"><?=$row["tengiay"]?></div>
        <div class="product-price"><?=number_format($row["gia"])?>₫</div>
        <div class="product-gender"><?=$row["gioitinh"]?></div>
        <div class="product-color">Màu: <?=$row["mau"]?></div>
        <?php endwhile; endif ?>
        <div class="product-buy-box center">
            <button class="product-buy btn center">
                <div class="product-buy-icon center">
                    <img src="../assets/icons/shopping-cart.svg">
                </div>
                Mua ngay
            </button>
        </div>
    </div>
</form>