<?php
    session_start();
    if(isset($_POST["view"])) {
        $_SESSION["page"] = $_POST["view"];
    }
    if(isset($_POST["filter"])) {
        $_SESSION["page"] = "list";
        $_SESSION["filter"] = $_POST["filter"];
    }
    if(isset($_POST["logout"])) {
        $_SESSION = array();
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
        session_destroy();
        header("Location: ../index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/root.css">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="<?="../css/".$_SESSION["page"].".css";?>">
</head>
<body>
    <div class="header center">
        <div class="header-logo center">
            <img src="../assets/icons/shoe.svg">
        </div>
        <form class="header-main" method="POST"> 
            <div class="header-acc center">
                <div class="spacer"></div>
                <?php
                    if (isset($_SESSION["username"])):
                ?>
                <button class="acc-login btn" name="logout">Đăng xuất</button>
                <div class="acc-name">
                    Xin chào, <?=$_SESSION["username"]?>!
                </div>
                <?php else: ?>
                <button class="acc-login btn" name="view" value="login">Đăng nhập</button>
                <?php endif; ?>
                <div class="header-acc-icon center">
                    <img src="../assets/icons/user.svg">
                </div>
            </div>
            <div class="header-nav center">
                <div class="nav-item">
                    <button class="nav-btn btn" name="filter" value="nam">Nam</button>
                    <button class="nav-btn btn" name="filter" value="nu">Nữ</button>
                    <button class="nav-btn btn" name="filter" value="sale">Sale</button>
                    <button class="nav-btn btn" name="filter" value="all">Tất cả</button>
                    <?php if(isset($_SESSION["username"]) && $_SESSION["username"] == "admin"): ?>
                    <button class="nav-btn btn" name="view" value="database">Quản lý</button>
                    <?php endif; ?>
                </div>
            </div>
        </form>
    </div>
    <div class="main">
        <?php
            require './'.$_SESSION["page"].'.php';
        ?>
    </div>
    <div class="footer">
        <div class="footer-item center">Chính sách</div>
        <div class="footer-item center">About Us</div>
        <div class="footer-item center">© 2023 Công ty TNHH Giày dép An Nam Việt Nam</div>
    </div>
</body>
</html>