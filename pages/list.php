<?php
    require '../utils/database.php';
    if(isset($_POST["product"])) {
        $_SESSION["page"] = "product";
        $_SESSION["product"] = $_POST["product"];
        header("Refresh:0");
    }
?>

<form class="list" method="POST">
    <?php
        $filter = "";
        if ($_SESSION["filter"] == "nam") {
            $filter = " WHERE gioitinh='Nam'";
        } elseif ($_SESSION["filter"] == "nu") {
            $filter = " WHERE gioitinh='Nữ'";
        } 
        $result = $db->query("SELECT * FROM giay{$filter}");

        if (mysqli_num_rows($result) > 0):
            while($row = $result->fetch_assoc()):
    ?>
    <button class="list-item btn" name="product" value="<?=$row["magiay"]?>">
        <div class="list-item-img center">
            <img src="../database/<?=$row["magiay"]?>.png">
        </div>
        <div class="list-item-text center">
            <?=$row["tengiay"]?>
        </div>
        <div class="list-item-text price center">
            <?=number_format($row["gia"])?>₫
        </div>
    </button>
    <?php endwhile; endif;?>
</form>