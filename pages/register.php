<?php
    require '../utils/database.php';
    if (isset($_POST["register"])) {
        if($db -> query("INSERT INTO `user`(`username`, `email`, `password`) VALUES ('{$_POST["username"]}', '{$_POST["email"]}', '{$_POST["password"]}')")) {
            $_SESSION["username"] = $_POST["username"];
            $_SESSION["page"] = "list";
            header("Refresh:0");
        }
    }
    if (isset($_POST["return"])) {
        $_SESSION["page"] = "login";
        header("Refresh:0");
    }
?>
<div class="register center">
    <form class="register-box center" method="POST">
        <button class="register-btn btn" name="return" formnovalidate>
            < Quay trở lại
        </button>
        <div class="register-logo">
            <img src="../assets/icons/shoe.svg">
        </div>
        <div class="register-info">
            <div class="register-title">Tên người dùng:</div> 
            <input class="register-input input" type="input" name="username" placeholder="Nhập tên người dùng" required>
            <div class="register-title">Email:</div> 
            <input class="register-input input" type="email" name="email" placeholder="Nhập email" required>
            <div class="register-title">Mật khẩu:</div> 
            <input class="register-input input" type="input" name="password" placeholder="Nhập mật khẩu" required>
        </div>
        <button class="register-btn btn" name="register">
            Đăng ký
        </button>
    </form>
</div>