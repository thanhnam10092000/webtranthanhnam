<?php
    require '../utils/database.php';
    if(isset($_POST["added"])) {
        if (isset($_FILES["img"])) {
            $file_name = $_FILES['img']['name'];
            $file_tmp =$_FILES['img']['tmp_name'];
            move_uploaded_file($file_tmp,"../database/{$_POST['magiay']}.png");

            $sql = "INSERT INTO giay (magiay,tengiay,gioitinh,mau,gia) VALUES ('{$_POST['magiay']}','{$_POST['tengiay']}','{$_POST['gioitinh']}','{$_POST['mau']}',{$_POST['gia']})";
            if ($db->query($sql) === TRUE) {
                $alert = "Thêm sản phẩm thành công";
                $alert_type = "success";
            }
        }
    }

    if(isset($_POST["edited"])) {
        $sql = "UPDATE giay SET tengiay='{$_POST['tengiay']}',gioitinh='{$_POST['gioitinh']}',mau='{$_POST['mau']}',gia={$_POST['gia']} WHERE magiay='{$_POST['edited']}'";
        if ($db->query($sql) === TRUE) {
            $alert = "Sửa sản phẩm thành công";
            $alert_type = "success";
        }
    }

    if(isset($_POST["delete"])) {
        unlink("../database/{$_POST['delete']}.png");
        $sql = "DELETE FROM giay WHERE magiay='{$_POST["delete"]}'";
        if ($db->query($sql) === TRUE) {
            $alert = "Xoá sản phẩm thành công";
            $alert_type = "success";
        }
    }
?>
<form class="main-box" method="POST" enctype="multipart/form-data">
    <div class="main-title">
        Danh sách sản phẩm
    </div>
    <div class="main-content">
        <div class="main-table">
            <div class="table-hd">Ảnh</div>
            <div class="table-hd">Mã giày</div>
            <div class="table-hd">Tên giày</div>
            <div class="table-hd">Giới tính</div>
            <div class="table-hd">Màu</div>
            <div class="table-hd">Giá</div>
            <div class="table-hd">Thao tác</div>
            <?php
                if(isset($_POST["add"])):
            ?>
                <div class="table-i">
                    <input name="img" type="file" id="img" style="display:none;" accept="image/*" required>
                    <label for="img" class="table-btn btn">Chọn ảnh</label>
                </div>
                <div class="table-i">
                    <input class="table-input input" name="magiay" required>
                </div>
                <div class="table-i">
                    <input class="table-input input" name="tengiay" required>
                </div>
                <div class="table-i">
                    <select class="table-input input" name="gioitinh" required>
                        <option value="Nam">Nam</option>
                        <option value="Nữ">Nữ</option>
                    </select>
                </div>
                <div class="table-i">
                    <input class="table-input input" name="mau" required>
                </div>
                <div class="table-i">
                    <input class="table-input input" name="gia" required>
                </div>
                <div class="table-i">
                    <button class="table-btn btn" name="added">Thêm</button>
                </div>
            <?php else: ?>
                <button class="table-add center btn" name="add">
                    <img class="table-add-icon" src="../assets/icons/plus-circle.svg">
                    Thêm sản phẩm
                </button>
            <?php endif ?>
            <?php
                $result = $db->query("SELECT * FROM giay");

                if (mysqli_num_rows($result) > 0):
                    while($row = $result->fetch_assoc()):
                        if(isset($_POST["edit"]) && $_POST["edit"] == $row["magiay"]):
            ?>
                <div class="table-i">
                    <img class="table-img" src="../database/<?=$row["magiay"]?>.png">
                </div>
                <div class="table-i"><?=$row["magiay"]?></div>
                <div class="table-i">
                    <input class="table-input input" name="tengiay" value="<?=$row["tengiay"]?>" required>
                </div>
                <div class="table-i">
                    <select class="table-input input" name="gioitinh" required>
                        <option value="Nam">Nam</option>
                        <option value="Nữ">Nữ</option>
                    </select>
                </div>
                <div class="table-i">
                    <input class="table-input input" name="mau" value="<?=$row["mau"]?>" required>
                </div>
                <div class="table-i">
                    <input class="table-input input" name="gia" value="<?=$row["gia"]?>" required>
                </div>
                <div class="table-i">
                    <button class="table-btn btn" name="edited" value=<?=$row["magiay"]?>>Sửa</button>
                </div>
            <?php else: ?>
                <div class="table-i">
                    <img class="table-img" src="../database/<?=$row["magiay"]?>.png">
                </div>
                <div class="table-i"><?=$row["magiay"]?></div>
                <div class="table-i"><?=$row["tengiay"]?></div>
                <div class="table-i"><?=$row["gioitinh"]?></div>
                <div class="table-i"><?=$row["mau"]?></div>
                <div class="table-i"><?=number_format($row["gia"])?>₫</div>
                <div class="table-i">
                    <button class="icon-box" name="edit" value="<?=$row["magiay"]?>">
                        <img src="../assets/icons/edit.svg" class="table-icon">
                    </button>
                    <button class="icon-box" name="delete" value="<?=$row["magiay"]?>">
                        <img src="../assets/icons/trash.svg" class="table-icon">
                    </button>
                </div>
            <?php endif; endwhile; endif; ?>
        </div>
    </div>
</form>