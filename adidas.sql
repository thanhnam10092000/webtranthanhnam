-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 01, 2023 at 04:01 PM
-- Server version: 5.7.11
-- PHP Version: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adidas`
--

-- --------------------------------------------------------

--
-- Table structure for table `giay`
--

CREATE TABLE `giay` (
  `magiay` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tengiay` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gioitinh` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mau` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gia` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `giay`
--

INSERT INTO `giay` (`magiay`, `tengiay`, `gioitinh`, `mau`, `gia`) VALUES
('ADSSB', 'Giày Superstar', 'Nam', 'Đen', 2600000),
('ADSSW', 'Giày Stan Smith trắng', 'Nam', 'Trắng', 2600000),
('GCCL', 'Giày Grand Court Cloudfoam Lifestyle', 'Nữ', 'Trắng vàng', 2000000),
('LR30', 'Giày Lite Racer 3.0', 'Nữ', 'Trắng hồng', 1600000),
('NMDR1', 'Giày NMD_R1 Primeblue', 'Nam', 'Đen', 3900000),
('RF3', 'Giày Runfalcon 3', 'Nữ', 'Trắng', 1800000),
('SYR', 'Giày Start Your Run', 'Nữ', 'Đen hồng', 1800000),
('U4DFWD', 'Giày Ultra 4DFWD', 'Nam', 'Đen', 6000000),
('UB10', 'Giày Ultraboost 1.0', 'Nữ', 'Trắng', 4500000),
('UB22', 'Giày Ultraboost 22 Made with Nature', 'Nam', 'Trắng', 5200000);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userid` int(11) NOT NULL,
  `username` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userid`, `username`, `email`, `password`) VALUES
(1, 'admin', 'admin@admin.com', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `giay`
--
ALTER TABLE `giay`
  ADD PRIMARY KEY (`magiay`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
